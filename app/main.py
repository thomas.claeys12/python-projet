# System imports

# Libs imports
from fastapi import FastAPI

# Local imports
from app.routers import users, entreprise, activity, planning, user_activity, notification
from app.internal import auth

app = FastAPI()

app.include_router(auth.router, tags=["Authentification"])
app.include_router(users.router, tags=["User"])
app.include_router(entreprise.router, tags=["Entreprise"])
app.include_router(activity.router, tags=["Activity"])
app.include_router(planning.router, tags=["Planning"])
app.include_router(user_activity.router, tags=["UserActivity"])
app.include_router(notification.router, tags=["Notification"])
