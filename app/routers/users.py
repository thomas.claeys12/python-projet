# System imports
from typing import Annotated

# Libs imports
from fastapi import APIRouter, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer
import bcrypt

# Local imports
from app.internal.models import User
from app.sql_app.connection_db import get_connection_db
from app.config.security import encrypt_data, decrypt_data
from app.internal.auth import decode_token

router = APIRouter()

cursor, connection = get_connection_db()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


@router.get('/users')
def read_users(userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role == 'Admin' or userAuth.role == 'Maintainer':
        cursor.execute('SELECT * FROM user')
        rows = cursor.fetchall()
        return [{'id': row[0], 'lastname': decrypt_data(row[1]), 'firstname': decrypt_data(row[2]), 'email': row[3],
                 'role': row[5]} for row in rows]
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')


@router.post('/users')
def create_user(user: User, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role == 'Admin' or userAuth.role == 'Maintainer':
        salt = bcrypt.gensalt()
        user.password = bcrypt.hashpw(user.password.encode('utf-8'), salt)
        user.lastname = encrypt_data(user.lastname)
        user.firstname = encrypt_data(user.firstname)
        cursor.execute('SELECT * FROM user WHERE email = ?', (user.email,))
        row = cursor.fetchone()
        if row:
            raise HTTPException(status_code=409, detail='User already exists')
        else:
            cursor.execute(
                'INSERT INTO user (lastname, firstname, email, password, role, entreprise_id) VALUES (?, ?, ?, '
                '?, ?, ?)',
                (user.lastname, user.firstname, user.email, user.password, user.role, user.entreprise_id))
            connection.commit()
            return {**user.dict()}
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')


@router.get('/users/{user_id}')
def read_user(user_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role == 'Admin' or userAuth.role == 'Maintainer':
        cursor.execute('SELECT * FROM user WHERE id = ?', (user_id,))
        row = cursor.fetchone()
        if row:
            return {'id': row[0], 'lastname': decrypt_data(row[1]), 'firstname': decrypt_data(row[2]), 'email': row[3],
                    'role': row[5]}
        else:
            raise HTTPException(status_code=404, detail='User not found')
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')


@router.put('/users/{user_id}')
def update_user(user_id: int, user: User, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role == 'Admin' or userAuth.role == 'Maintainer' or userAuth.id == user_id:
        password_hash = bcrypt.hashpw(user.password.encode('utf-8'), bcrypt.gensalt())
        cursor.execute(
            'UPDATE user SET lastname = ?, firstname = ?, email = ?, password = ?, role = ?, entreprise_id = ? WHERE id = ?',
            (user.lastname, user.firstname, user.email, password_hash, user.role, user.entreprise_id, user_id))
        connection.commit()
        if cursor.rowcount:
            return {'id': user_id, **user.dict()}
        else:
            raise HTTPException(status_code=404, detail='User not found')
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')


@router.delete('/users/{user_id}')
def delete_user(user_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role == 'Admin' or userAuth.role == 'Maintainer' or userAuth.id == user_id:
        cursor.execute('DELETE FROM user WHERE id = ?', (user_id,))
        connection.commit()
        return {'message': 'User deleted'}
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')
