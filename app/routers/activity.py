# System imports
import hashlib
from typing import Annotated

import bcrypt
# Libs imports
from fastapi import APIRouter, status, Response, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer

# Local imports
from app.internal.models import User, Activity
from app.internal.auth import decode_token
from app.sql_app.connection_db import get_connection_db
from app.config.security import decrypt_data

router = APIRouter()

cursor, connection = get_connection_db()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


@router.get('/activity')
def read_activity(userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT activity.id, activity.name, activity.description, activity.start_time, activity.end_time, '
                   'activity.planning_id, user.firstname, user.lastname FROM activity LEFT JOIN user ON user.id = '
                   'activity.createdBy')
    rows = cursor.fetchall()
    print(rows)
    i = 0
    for row in rows:
        if userAuth.role == 'Admin' or userAuth.role == 'Maintainer':
            cursor.execute(
                'SELECT user.firstname, user.lastname FROM user_activity INNER JOIN user ON user.id = '
                'user_activity.user_id WHERE user_activity.activity_id = ? AND user.entreprise_id = ?',
                (row[0], userAuth.entreprise_id))
            users = cursor.fetchall()
            row = list(row)
            tabUser = []
            for user in users:
                user2 = list(user)
                tabUser.append(decrypt_data(user2[0]) + ' ' + decrypt_data(user2[1]))
            row.append(tabUser)
        else:
            cursor.execute('SELECT COUNT(*) FROM user_activity WHERE user_activity.activity_id = ?', (row[0],))
            nb_users = cursor.fetchone()
            row = list(row)
            row.append(nb_users[0])
        rows[i] = row
        i += 1
        print(rows)
    return [{'id': row[0], 'name': row[1], 'description': row[2], 'start_time': row[3], 'end_time': row[4],
             'planning_id': row[5], 'createdBy': decrypt_data(row[6]) + ' ' + decrypt_data(row[7]), 'user': row[8]} for row in
            rows]


@router.post('/activity')
def create_activity(activity: Activity, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role == 'Admin' or userAuth.role == 'Maintainer':
        cursor.execute(
            'INSERT INTO activity (name, description,createdBy, start_time, end_time, planning_id) VALUES (?,?,?,?,?,?)',
            (activity.name, activity.description, activity.createdBy, activity.start_time, activity.end_time,
             activity.planning_id))
        connection.commit()
        return {'id': cursor.lastrowid, **activity.dict()}
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')

@router.get('/activity/{activity_id}')
def read_activity(activity_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT activity.id, activity.name, activity.description, activity.start_time, activity.end_time, '
                   'activity.planning_id, user.firstname, user.lastname FROM activity LEFT JOIN user ON user.id = '
                   'activity.createdBy WHERE activity.id = ?', (activity_id,))
    row = cursor.fetchone()
    if row:
        if userAuth.role == 'Admin' or userAuth.role == 'Maintainer':
            cursor.execute(
                'SELECT user.firstname, user.lastname FROM user_activity INNER JOIN user ON user.id = '
                'user_activity.user_id WHERE user_activity.activity_id = ? AND user.entreprise_id = ?',
                (row[0], userAuth.entreprise_id))
            users = cursor.fetchall()
            row = list(row)
            tabUser = []
            for user in users:
                user2 = list(user)
                tabUser.append(decrypt_data(user2[0]) + ' ' + decrypt_data(user2[1]))
            row.append(tabUser)
        else:
            cursor.execute('SELECT COUNT(*) FROM user_activity WHERE user_activity.activity_id = ?', (row[0],))
            nb_users = cursor.fetchone()
            row = list(row)
            row.append(nb_users[0])
        return {'id': row[0], 'name': row[1], 'description': row[2], 'start_time': row[3], 'end_time': row[4],
                'planning_id': row[5],'createdBy': decrypt_data(row[6]) + ' ' + decrypt_data(row[7]),  'user': row[8]}
    else:
        raise HTTPException(status_code=404, detail='Activity not found')


@router.put('/activity/{activity_id}')
def update_activity(activity_id: int, activity: Activity, userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM activity WHERE id = ?', (activity_id,))
    row = cursor.fetchone()
    if userAuth.role == 'Admin' or userAuth.role == 'Maintainer' or row[6] == userAuth.id:
        if row:
            cursor.execute(
                'UPDATE activity SET name = ?, description = ?, start_time = ?, end_time = ?, planning_id = ? WHERE '
                'id = ?',
                (activity.name, activity.description, activity.start_time, activity.end_time, activity.planning_id,
                 activity_id))
            connection.commit()
            cursor.execute('SELECT user_id FROM user_activity WHERE activity_id = ?', (activity_id,))
            users = cursor.fetchall()
            for user in users:
                cursor.execute('INSERT INTO notification (status, message, user_id, activity_id) VALUES (?,?,?,?)', (False, 'The activity ' + activity.name + ' has been modified', user[0], activity_id))
                connection.commit()
            return {'id': activity_id, **activity.dict()}
        else:
            raise HTTPException(status_code=404, detail='Activity not found')
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')


@router.delete('/activity/{activity_id}')
def delete_activity(activity_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM activity WHERE id = ?', (activity_id,))
    row = cursor.fetchone()
    if userAuth.role == 'Admin' or userAuth.role == 'Maintainer' or row[6] == userAuth.id:
        cursor.execute('SELECT * FROM activity WHERE id = ?', (activity_id,))
        row = cursor.fetchone()
        if row:
            cursor.execute('DELETE FROM activity WHERE id = ?', (activity_id,))
            connection.commit()
            return Response(status_code=status.HTTP_204_NO_CONTENT)
        else:
            raise HTTPException(status_code=404, detail='Activity not found')
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')