# System imports
import hashlib
from typing import Annotated

import bcrypt
# Libs imports
from fastapi import APIRouter, status, Response, HTTPException, Depends
import sqlite3
from fastapi.security import OAuth2PasswordBearer

# Local imports
from app.internal.models import User, Entreprise
from app.internal.auth import decode_token
from app.sql_app.connection_db import get_connection_db

router = APIRouter()

cursor, connection = get_connection_db()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


@router.get('/entreprises')
def read_entreprises(userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role != 'Maintainer':
        raise HTTPException(status_code=401, detail='Unauthorized')
    cursor.execute('SELECT * FROM entreprise')
    rows = cursor.fetchall()
    return [{'id': row[0], 'name': row[1]} for row in rows]


@router.post('/entreprises')
def create_entreprise(entreprise: Entreprise, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role != 'Maintainer':
        raise HTTPException(status_code=401, detail='Unauthorized')
    cursor.execute('SELECT * FROM entreprise WHERE name = ?', (entreprise.name,))
    row = cursor.fetchone()
    if row:
        raise HTTPException(status_code=409, detail='Entreprise already exists')
    else:
        cursor.execute('INSERT INTO entreprise (name) VALUES (?)',
                       (entreprise.name,))
        connection.commit()
        return {'id': cursor.lastrowid, **entreprise.dict()}


@router.get('/entreprises/{entreprise_id}')
def read_entreprise(entreprise_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.entreprise_id != entreprise_id:
        raise HTTPException(status_code=401, detail='Unauthorized')
    cursor.execute('SELECT * FROM entreprise WHERE id = ?', (entreprise_id,))
    row = cursor.fetchone()
    if row:
        return {'id': row[0], 'name': row[1]}
    else:
        raise HTTPException(status_code=404, detail='Entreprise not found')


@router.put('/entreprises/{entreprise_id}')
def update_entreprise(entreprise_id: int, entreprise: Entreprise, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role != 'Maintainer':
        raise HTTPException(status_code=401, detail='Unauthorized')
    cursor.execute('SELECT * FROM entreprise WHERE id = ?', (entreprise_id,))
    row = cursor.fetchone()
    if row:
        cursor.execute('UPDATE entreprise SET name = ? WHERE id = ?', (entreprise.name, entreprise_id))
        connection.commit()
        return {'id': entreprise_id, **entreprise.dict()}
    else:
        raise HTTPException(status_code=404, detail='Entreprise not found')


@router.delete('/entreprises/{entreprise_id}')
def delete_entreprise(entreprise_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role != 'Maintainer':
        raise HTTPException(status_code=401, detail='Unauthorized')
    cursor.execute('SELECT * FROM entreprise WHERE id = ?', (entreprise_id,))
    row = cursor.fetchone()
    if row:
        cursor.execute('DELETE FROM entreprise WHERE id = ?', (entreprise_id,))
        connection.commit()
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    else:
        raise HTTPException(status_code=404, detail='Entreprise not found')
