# System imports
from typing import Annotated

# Libs imports
from fastapi import APIRouter, status, Response, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer

# Local imports
from app.internal.models import User, Planning
from app.internal.auth import decode_token
from app.sql_app.connection_db import get_connection_db

router = APIRouter()

cursor, connection = get_connection_db()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


@router.get('/plannings')
def read_plannings(userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM planning WHERE entreprise_id = ?', (userAuth.entreprise_id,))
    rows = cursor.fetchall()
    return [{'id': row[0], 'name': row[1], 'active': row[2], 'entreprise_id': row[3]} for row in rows]


@router.post('/plannings')
def create_planning(planning: Planning, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role == 'Admin' or userAuth.role == 'Maintainer':
        cursor.execute('INSERT INTO planning (name, active, entreprise_id) VALUES (?, ?, ?)',
                      (planning.name, planning.active, planning.entreprise_id))
        connection.commit()
        return {'id': cursor.lastrowid, **planning.dict()}
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')


@router.get('/plannings/{planning_id}')
def read_planning(planning_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM planning WHERE id = ? AND entreprise_id = ?', (planning_id,userAuth.entreprise_id))
    row = cursor.fetchone()
    if row:
        if row[3] != userAuth.entreprise_id:
            raise HTTPException(status_code=401, detail='Unauthorized')
        return {'id': row[0], 'name': row[1], 'active': row[2], 'entreprise_id': row[3]}
    else:
        raise HTTPException(status_code=404, detail='Planning not found')


@router.put('/plannings/{planning_id}')
def update_planning(planning_id: int, planning: Planning, userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM planning WHERE id = ?', (planning_id,))
    row = cursor.fetchone()
    if row:
        if (row[3] != userAuth.entreprise_id and userAuth.role != 'Admin') or userAuth.role != 'Maintainer':
            raise HTTPException(status_code=401, detail='Unauthorized')
        cursor.execute('UPDATE planning SET name = ?, active = ?, entreprise_id = ? WHERE id = ?', (planning.name, planning.active, planning.entreprise_id, planning_id))
        connection.commit()
        return {'id': planning_id, **planning.dict()}
    else:
        raise HTTPException(status_code=404, detail='Planning not found')


@router.delete('/plannings/{planning_id}')
def delete_planning(planning_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM planning WHERE id = ?', (planning_id,))
    row = cursor.fetchone()
    if row:
        if (row[3] != userAuth.entreprise_id and userAuth.role != 'Admin') or userAuth.role != 'Maintainer':
            raise HTTPException(status_code=401, detail='Unauthorized')
        cursor.execute('DELETE FROM planning WHERE id = ?', (planning_id,))
        connection.commit()
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    else:
        raise HTTPException(status_code=404, detail='Planning not found')




