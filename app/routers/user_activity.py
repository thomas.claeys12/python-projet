# System imports
from typing import Annotated

import bcrypt
# Libs imports
from fastapi import APIRouter, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer

# Local imports
from app.internal.models import User, UserActivity
from app.internal.auth import decode_token
from app.sql_app.connection_db import get_connection_db

router = APIRouter()

cursor, connection = get_connection_db()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


# CRUD for user_activity table with user_id and activity_id

@router.get('/user_activity')
def read_user_activity(userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM user_activity')
    rows = cursor.fetchall()
    return [{'id': row[0], 'user_id': row[1], 'activity_id': row[2]} for row in rows]


@router.post('/user_activity')
def create_user_activity(user_activity: UserActivity, userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM user_activity WHERE user_id = ? AND activity_id = ?',
                   (user_activity.user_id, user_activity.activity_id))
    row = cursor.fetchone()
    if row:
        raise HTTPException(status_code=409, detail='User is already registered in this activity')
    else:
        cursor.execute('SELECT * FROM activity WHERE id = ?', (user_activity.activity_id,))
        row = cursor.fetchone()
        if row:
            cursor.execute('SELECT * FROM planning WHERE id = ?', (row[5],))
            row = cursor.fetchone()
            if row:
                if row[3] != userAuth.entreprise_id:
                    raise HTTPException(status_code=401, detail="Cette activité n'appartient pas à votre entreprise")
            else:
                raise HTTPException(status_code=404, detail='planning not found')
        else:
            raise HTTPException(status_code=404, detail='Activity not found')
        cursor.execute('INSERT INTO user_activity (user_id, activity_id) VALUES (?,?)',
                       (user_activity.user_id, user_activity.activity_id))
        connection.commit()
        return {'id': cursor.lastrowid, **user_activity.dict()}


@router.get('/user_activity/{user_activity_id}')
def read_user_activity(user_activity_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role != 'Maintainer':
        raise HTTPException(status_code=401, detail="Vous n'avez pas le droit de voir cette activité")
    cursor.execute('SELECT * FROM user_activity WHERE id = ?', (user_activity_id,))
    row = cursor.fetchone()
    if row:
        return {'id': row[0], 'user_id': row[1], 'activity_id': row[2]}
    else:
        raise HTTPException(status_code=404, detail='User_activity not found')


@router.delete('/user_activity/{user_activity_id}')
def delete_user_activity(user_activity_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM user_activity WHERE id = ?', (user_activity_id,))
    row = cursor.fetchone()
    if row:
        if userAuth.role == 'Admin' or userAuth.role == 'Maintainer' or userAuth.id == row[1]:
            cursor.execute('DELETE FROM user_activity WHERE id = ?', (user_activity_id,))
            connection.commit()
            cursor.execute('INSERT INTO notification (status,message, user_id, activity_id) VALUES (?,?,?,?)',
                           (False, "Vous avez quitté l'activité", row[1], row[2]))
            connection.commit()
            return {'message': 'Vous avez quitté l\'activité'}
        else:
            raise HTTPException(status_code=401, detail="Vous n'avez pas le droit de supprimer cette activité")
    else:
        raise HTTPException(status_code=404, detail='User_activity not found')

