# System imports
from typing import Annotated

import bcrypt
# Libs imports
from fastapi import APIRouter, status, Response, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer

# Local imports
from app.internal.models import User, Notification
from app.internal.auth import decode_token
from app.sql_app.connection_db import get_connection_db

router = APIRouter()

cursor, connection = get_connection_db()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


@router.get('/notifications')
def read_notification(userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role != 'Maintainer':
        raise HTTPException(status_code=401, detail='Unauthorized')
    cursor.execute('SELECT * FROM notification')
    rows = cursor.fetchall()
    return [{'id': row[0], 'status': row[1], 'message': row[2], 'user_id': row[3], 'activity_id': row[4]} for row in
            rows]


@router.post('/notifications')
def create_notification(notification: Notification, userAuth: Annotated[User, Depends(decode_token)]):
    if userAuth.role != 'Maintainer':
        raise HTTPException(status_code=401, detail='Unauthorized')
    cursor.execute('INSERT INTO notification (status,message, user_id, activity_id) VALUES (?,?,?,?)',
                   (notification.status, notification.message, notification.user_id, notification.activity_id))
    connection.commit()
    return {'id': cursor.lastrowid, **notification.dict()}


@router.get('/notifications/{notification_id}')
def read_notification(notification_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM notification WHERE id = ?', (notification_id,))
    row = cursor.fetchone()
    if row:
        if userAuth.id != row[3]:
            raise HTTPException(status_code=401, detail='Unauthorized')
        cursor.execute('UPDATE notification SET status = ? WHERE id = ?', (True, notification_id))
        connection.commit()
        return {'id': row[0], 'status': row[1], 'message': row[2], 'user_id': row[3], 'activity_id': row[4]}
    else:
        raise HTTPException(status_code=404, detail='Notification not found')


@router.put('/notifications/{notification_id}')
def update_notification(notification_id: int, notification: Notification,
                        userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM notification WHERE id = ?', (notification_id,))
    row = cursor.fetchone()
    if row:
        if userAuth.id != row[3]:
            raise HTTPException(status_code=401, detail='Unauthorized')
        cursor.execute('UPDATE notification SET status = ?, user_id = ?, activity_id = ? WHERE id = ?',
                       (notification.status, notification.user_id, notification.activity_id, notification_id))
        connection.commit()
        return {'id': notification_id, **notification.dict()}
    else:
        raise HTTPException(status_code=404, detail='Notification not found')


@router.delete('/notifications/{notification_id}')
def delete_notification(notification_id: int, userAuth: Annotated[User, Depends(decode_token)]):
    cursor.execute('SELECT * FROM notification WHERE id = ?', (notification_id,))
    row = cursor.fetchone()
    if row:
        if userAuth.id != row[3]:
            raise HTTPException(status_code=401, detail='Unauthorized')
        cursor.execute('DELETE FROM notification WHERE id = ?', (notification_id,))
        connection.commit()
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    else:
        raise HTTPException(status_code=404, detail='Notification not found')
