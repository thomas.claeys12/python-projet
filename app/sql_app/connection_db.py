import sqlite3

connection = sqlite3.connect('app/sql_app/bd', check_same_thread=False)
cursor = connection.cursor()
cursor.execute('PRAGMA foreign_keys = ON;')


def get_connection_db():
    return cursor, connection

