PRAGMA foreign_keys = OFF;

CREATE TABLE entreprise
(
    id   INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(100) NOT NULL
);

CREATE TABLE user
(
    id            INTEGER PRIMARY KEY AUTOINCREMENT,
    lastname      VARCHAR(50)  NOT NULL,
    firstname     VARCHAR(50)  NOT NULL,
    email         VARCHAR(100) NOT NULL UNIQUE,
    password      VARCHAR(100) NOT NULL,
    role          VARCHAR(50)  NOT NULL,
    entreprise_id INTEGER NULL,
        CONSTRAINT fk_user_entreprise
    FOREIGN KEY (entreprise_id) REFERENCES entreprise(id)
);

CREATE TABLE planning
(
    id            INTEGER PRIMARY KEY AUTOINCREMENT,
    name          VARCHAR(100) NOT NULL,
    active        BOOLEAN      NOT NULL,
    entreprise_id INTEGER NOT NULL,
        CONSTRAINT fk_planning_entreprise
    FOREIGN KEY (entreprise_id) REFERENCES entreprise(id)
);

CREATE TABLE activity
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        VARCHAR(100)  NOT NULL,
    description VARCHAR(1000) NOT NULL,
    start_time  TIMESTAMP     NOT NULL,
    end_time    TIMESTAMP     NOT NULL,
    planning_id INTEGER NOT NULL,
    createdBy   INTEGER NULL,
        CONSTRAINT fk_activity_planning
    FOREIGN KEY (planning_id) REFERENCES planning(id),
        CONSTRAINT fk_activity_user
    FOREIGN KEY (createdBy) REFERENCES user(id)
);

CREATE TABLE user_activity
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER NOT NULL,
    activity_id INTEGER NOT NULL,
        CONSTRAINT fk_user_activity_user
    FOREIGN KEY (user_id) REFERENCES user(id),
        CONSTRAINT fk_user_activity_activity
    FOREIGN KEY (activity_id) REFERENCES activity(id)
);

CREATE TABLE notification
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    status      BOOLEAN NOT NULL,
    message     VARCHAR(1000) NULL,
    user_id     INTEGER NOT NULL,
    activity_id INTEGER NOT NULL,
      CONSTRAINT fk_notification_user
    FOREIGN KEY (user_id) REFERENCES user(id),
          CONSTRAINT fk_notification_activity
    FOREIGN KEY (activity_id) REFERENCES activity(id)
);

PRAGMA foreign_keys = ON;