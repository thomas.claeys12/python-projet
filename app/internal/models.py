# System imports

# Libs imports
from pydantic import BaseModel


# Local imports

class User(BaseModel):
    id: int = None
    lastname: str
    firstname: str
    email: str
    password: str
    role: str
    entreprise_id: int = None


class Activity(BaseModel):
    name: str
    start_time: str
    end_time: str
    createdBy: int
    description: str
    planning_id: int


class UserActivity(BaseModel):
    user_id: int
    activity_id: int


class Entreprise(BaseModel):
    name: str

class Notification(BaseModel):
    status: bool
    user_id: int
    message: str
    activity_id: int


class Planning(BaseModel):
    name: str
    active: bool
    entreprise_id: int

