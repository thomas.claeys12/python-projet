# System imports
import sqlite3
from typing import Annotated
import hashlib

import bcrypt
# Libs imports
from fastapi import Depends, APIRouter, status, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt

# Local imports
from app.internal.models import User
from app.sql_app.connection_db import get_connection_db

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

cursor, connection = get_connection_db()

JWT_KEY = "kajshkdalasjjlhgkjguifoudhsfkxahdsf"


async def decode_token(token: Annotated[str, Depends(oauth2_scheme)]) -> User:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        decoded_data = jwt.decode(token, JWT_KEY, algorithms=['HS256'])
        cursor.execute('SELECT * FROM user WHERE email = ?', (decoded_data['email'],))
        row = cursor.fetchone()
        if row:
            return User(id=row[0], lastname=row[1], firstname=row[2], email=row[3], password=row[4], role=row[5],
                        entreprise_id=row[6])
        else:
            raise credentials_exception
    except JWTError:
        return credentials_exception


@router.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    cursor.execute('SELECT * FROM user WHERE email = ?', (form_data.username,))
    row = cursor.fetchone()
    if row:
        if bcrypt.checkpw(form_data.password.encode('utf-8'), row[4]):
            encoded_jwt = jwt.encode({"email": form_data.username}, JWT_KEY, algorithm="HS256")
            return {"access_token": encoded_jwt, "token_type": "bearer"}
        else:
            raise HTTPException(status_code=401, detail='Invalid email or password')
    else:
        raise HTTPException(status_code=401, detail='Invalid email or password')
