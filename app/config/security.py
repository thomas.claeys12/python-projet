from cryptography.fernet import Fernet


key = b'PSn4sBHRI5XcrqarEHdCYGtYxDEreCl9XSN6LUoPuj4='
fernet = Fernet(key)


def encrypt_data(data):
    return fernet.encrypt(data.encode('utf-8'))


def decrypt_data(data):
    return fernet.decrypt(data).decode()
