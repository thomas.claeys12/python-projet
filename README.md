# Projet Back python thomas Claeys
Projet back fastApi

## Uvicorn mode
**requirements:** python3.11 or greater installed  
First, install dependencies
`pip install -r requirements.txt`

Then, run the app:  
`uvicorn app.main:app --reload`

Pour la base de donnée, elle se situe dans le dossier sql_app: bd
tout est initialisé dedans (table, données)

table user:

| ID | lastname | firstname | Email                     | password | role       | entreprise_id |
|----|----------|-----------|---------------------------|----------|------------|---------------|
| 1  | thomas   | cls       | maintainer@maintainer.com | azerty   | Maintainer | 1             |
| 2  | thomas   | admin     | admin@admin.com           | azerty   | Admin      | 1             |
| 3  | thomas   | user      | user@user.fr              | azerty   | User       | 1             |

table entreprise:

| ID | name |
|----|------|
| 1  | acer |

table activity:

| ID | name      | description                        | start_time               | end_time                | planning_id | createdBy |
|----|-----------|------------------------------------|--------------------------|-------------------------|-------------|-----------|
| 1  | parapente | on va faire du parapente lets gooo | 2023-05-17 11:23:39.000  | 2023-06-17 11:23:39.000 | 1           | 1         |

table planning:

| ID | name       | active | entreprise_id |
|----|------------|--------|---------------|
| 1  | planning_1 | True   | 1             |

table user_activity:

| ID  | user_id | activity_id |
|-----|---------|-------------|
| 1   | 1       | 1           |
| 2   | 2       | 1           |
| 3   | 3       | 1           |


